<?php
$pageTitle = 'Ajouter une bouteille';
require_once('src/controllers/bouteils_add.php');

ob_start();
?>

<div class="containerFormAddEdite">
    <h1 class="text">Ajouter une bouteille</h1>
    <form action="bouteils_add.php" method="post" enctype="multipart/form-data">
        <div class="division">
            <div>

                <div class="form-floating">
                    <input type="text" class="form-control" id="floatingName" name="name" placeholder="Nom">
                </div>
                <br>
                <?php if(isset($errorName)){echo $errorName;} ?>

                <div class="form-floating">
                    <input type="number" class="form-control" id="floatingYear" name="year" placeholder="Année">
                </div>
                <br>
                <div class="form-floating">
                    <input type="text" class="form-control" id="floatingGrapes" name="grapes" placeholder="Grappes">
                </div>
                    <?php if(isset($errorGrappes)){echo $errorGrappes;} ?>
                <br>
                <div class="form-floating">
                    <input type="text" class="form-control" id="floatingCountry" name="country" placeholder="Pays">
                </div>
                    <?php if(isset($errorCountry)){echo $errorCountry;} ?>
                <br>
                <div class="form-floating">
                    <input type="text" class="form-control" id="floatingRegion" name="region" placeholder="Régions">
                </div>
                    <?php if(isset($errorRegion)){echo $errorRegion;} ?>
            </div>

            <div class="imgDescp">

                <div class="addImg">
                    <input class="form-control" type="file" id="image" name="images">
                </div>
                <br>
                <div class="form-floating">
                    <textarea class="form-control" placeholder="Description" id="floatingDescription" name="description"
                        style="height: 100px"></textarea>
                </div>
            </div>


            <?php if(isset($errorImageWeight)){echo $errorImageWeight;} ?>
            <?php if(isset($errorExtention)){echo $errorExtention;} ?>
            <button type="submit" name="creer" class="btn">Créer</button>
        </div>
    </form>
</div>

<?php
$content = ob_get_clean();
require_once('templates/layout.php');
?>