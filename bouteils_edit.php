<?php
$pageTitle = 'Editer une bouteille';
require_once('src/controllers/bouteils_edit.php');

ob_start();
?>

<div class="containerFormAddEdite">
    <h1 class="text">Editer une bouteille</h1>
    <?php if(isset($msgError)) { ?>
        <span class="alert alert-danger mb-3"><?php echo $msgError ?></span>
    <?php } ?>
    <form action="bouteils_edit.php?id_vins=<?php echo $vins['id'] ?>" method="post" enctype="multipart/form-data">
        <div class="division">
            <div class="mb-3">
                <img src="public/img/<?php echo $vins['images'] ?>" alt="photo">
            </div>

            <div>
                <div class="form-floating mb-3">
                    <input type="text" class="form-control" id="floatingName" value="<?php echo $vins['name'] ?>" name="name" placeholder="<?php echo $vins['name'] ?>">
                </div>
                <?php if(isset($errorName)){echo $errorName;} ?>
                <br>
                <div class="form-floating">
                    <input type="number" class="form-control" id="floatingYear" name="year" placeholder="<?php echo $vins['year'] ?>">
                </div>
                <br>
                <div class="form-floating">
                    <input type="text" class="form-control" id="floatingGrapes" name="grapes" placeholder="<?php echo $vins['grapes'] ?>">
                </div>
                <?php if(isset($errorGrappes)){echo $errorGrappes;} ?>
                <br>
                <div class="form-floating">
                    <input type="text" class="form-control" id="floatingCountry" name="country" placeholder="<?php echo $vins['country'] ?>">
                </div>
                <?php if(isset($errorCountry)){echo $errorCountry;} ?>
                <br>
                <div class="form-floating">
                    <input type="text" class="form-control" id="floatingRegion" name="region" placeholder="<?php echo $vins['region'] ?>">
                </div>
                <?php if(isset($errorRegion)){echo $errorRegion;} ?>
            </div>

            <div class="imgDescp">
                <div class="mt-3">
                    <input class="form-control" type="file" id="image" name="images">
                </div>
                <br>
                <div class="form-floating mt-3">
                    <textarea class="form-control" placeholder="Description" id="floatingDescription" name="description" style="height: 100px"><?php echo $vins['description'] ?></textarea>
                </div>
            </div>
            <?php if(isset($errorExtention)){echo $errorExtention;} ?>
            <button type="submit" name="modifier" class="btn btn-primary mt-3">Modifier</button>
        </div>
    </form>
</div>


<?php
$content = ob_get_clean();
require_once('templates/layout.php');
?>