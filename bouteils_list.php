<?php
if(session_status()!=2) {
    
    session_start();
}
$pageTitle = 'Liste des vins';
require_once('src/models/vins.php');
$vins = getvinsHome();
ob_start();
?>

<?php if(isset($_SESSION['utilisateurs'])){ ?>
<a href="bouteils_add.php" class="btn ">Ajouter une bouteille</a>
<?php } ?>

    <div class="bgCards">
    <?php foreach ($vins as $vins) : ?>
        <div class="cardsContainer">
            <div class="header">
                <div class="naImg">
                    <img class="imgC" src="public/img/<?php echo $vins['images'] ?>" alt="images">
                </div>
                <h3><?php echo $vins['name'] ?></h3>
            </div>
            <div class="mainCards">
                <div class="division">
                    <div class="h4">
                        <h4>Année : <em><?php echo $vins['year'] ?></em></h4>
                    </div>
                    <div class="h4">
                        <h4>Pays : <em><?php echo $vins['country'] ?></em></h4>
                    </div>
                </div>
                <div class="liens">
                    <a href="wine_detail.php?id_vins=<?php echo $vins['id'] ?>" class="btn"><i class="fas fa-eye"></i></a>
                    <?php if(isset($_SESSION['utilisateurs'])){ ?>
                    <a href="bouteils_edit.php?id_vins=<?php echo $vins['id'] ?>" class="btn"><i class="fas fa-edit"></i></a>
                    <?php } ?>
                    <?php if(isset($_SESSION['utilisateurs'])){ ?>
                    <a href="src/controllers/bouteils_del.php?id_vins=<?php echo $vins['id'] ?>" class="btn"><i class="fas fa-trash-alt"></i></a>
                    <?php } ?>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
    </div>

<?php 
$content = ob_get_clean();
require_once('templates/layout.php'); 
?>