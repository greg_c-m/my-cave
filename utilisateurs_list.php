<?php
if(session_status()!=2) {
    
    session_start();
}
$pageTitle = 'Listes des utilisateurs';
require_once('src/models/utilisateurs.php');

$utilisateurs = getUtilisateursList();
ob_start();
?>

 <?php if(isset($_SESSION['utilisateurs'])){ ?>
<a href="utilisateurs_add.php" class="btn ">Ajouter un utilisateur</a>
<?php } ?>

<div class="bgCards">
    <?php foreach ($utilisateurs as $utilisateurs) : ?>
        <div class="cardsContainer">
            <div class="header">
                <div class="naImg">
                    <!-- <img class="imgC" src="public/img/<?php echo $vins['images'] ?>" alt="images"> -->
                    <h3>Nom : <?php echo $utilisateurs['name'] ?></h3>
                </div>
            </div>
            <div class="mainCards">
                <div class="division">
                    <div class="anGrap">
                        <h4>E-mail : <?php echo $utilisateurs['mail'] ?></h4>
                    </div>
                </div>
                <div class="liens">
                    <a href="utilisateurs_edit.php?id_utilisateurs=<?php echo $utilisateurs['id'] ?>" class="btn"><i class="fas fa-edit"></i>
                    </a>
                    <a href="./src/controllers/utilisateurs_del.php?id_utilisateur=<?php echo $utilisateurs['id'] ?>" class="btn"><i class="fas fa-trash-alt"></i>
                    </a>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
    </div>


<?php
$content = ob_get_clean();
require_once('templates/layout.php');
?>