<!DOCTYPE html>
<html lang="fr">
<?php
if(session_status()!=2) {
    
    session_start();
}
require_once('src/controllers/acl.php');
$pageTitle = $pageTitle ?? '';
?>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>My Cave | <?php echo $pageTitle ?></title>
        <link rel="stylesheet" href="./public/css/root.css">
        <link rel="stylesheet" href="./public/css/reboot.css">
        <link rel="stylesheet" href="./public/css/style.css">
        <link rel="stylesheet" href="./public/css/details.css">
        <link rel="stylesheet" href="./public/css/utilisateurAddEdite.css">
        <link rel="stylesheet" href="./public/css/formAddEdite.css">
        <link rel="stylesheet" href="./public/css/menuBurger.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        
    </head>
    <body>
        <header>
            <!-- NavBar -->
            <div class="navbar1">
                <div class="bg">
                    <div class="navbar">
                        <div class="logoBlock">
                            <a href="index.php"><img src="./public/img/logo-large.png" alt="logo" class="logo"></a>
                        </div>
                        
                        <div class="cadrage">
                            <a class="btn" href="bouteils_cards.php">Toutes nos bouteils</a>
                        </div>
    
                        <div class="dropdown">
                            <button class="btn">Administration</button>
                            <div class="dropdown-content">
                                <a href="bouteils_list.php">listes des bouteilles</a>
                                <?php if(isset($_SESSION['utilisateurs'])){ ?>
                                <a href="bouteils_add.php">Ajouter une bouteille</a>
                                <?php } ?>
                                <?php if(isset($_SESSION['utilisateurs'])){ ?>
                                <a href="utilisateurs_list.php">Liste utilisateurs</a>
                                <?php } ?>
                            </div>
                        </div>
                        
                        <div class="btnConnection">
                            <?php if(isset($_SESSION['utilisateurs'])){ ?>
                            <a class="btn" aria-current="page" href="src/controllers/logout.php">Se déconnecter</a>
                            <?php }else{ ?>
                            <a class="btn" aria-current="page" href="login.php">Se connecter</a>
                            <?php } ?>
                        </div>
                    </div>
                </div> 
    
                <!-- differents pays pour filtre -->
                <div class="country">
                    <div class="france"><a href="france.php">France</a></div>
                    <div class="usa"><a href="usa.php">Etats Unis</a></div>
                    <div class="autre"><a href="autres.php">Autres Pays</a></div>
                </div>
            </div>

            <!-- Menu Burger -->
            <nav>
                <ul class="pushNav js-topPushNav">
                    <li class="closeLevel js-closeLevelTop hdg">
                    <i class="fa fa-close"></i>
                    Close
                    </li>
                    <li>
                    <a href="#">
                        <i class="fa fa-home"></i>
                        Home
                    </a>
                    </li>
                    
                    <li><!-- Begin section 1 -->
                    <div class="openLevel js-openLevel">
                        Section 1 
                        <i class="fa fa-chevron-right"></i>
                    </div>
                    <ul class="pushNav pushNav_level js-pushNavLevel">
                        <li class="closeLevel js-closeLevel hdg">
                        <i class="fa fa-chevron-left"></i>
                        Go Back
                        </li>
                        <li>
                        <div class="openLevel js-openLevel">
                            Section 1.1
                            <i class="fa fa-chevron-right"></i>
                        </div>
                        <ul class="pushNav pushNav_level js-pushNavLevel">
                            <li class="closeLevel js-closeLevel hdg">
                            <i class="fa fa-chevron-left"></i>
                            Go Back
                            </li>
                            <li><a href="#">Link to page five</a></li>
                            <li><a href="#">Link to page six</a></li>
                            <li><a href="#">Link to page seven</a></li>
                            <li><a href="#">Link to page eight</a></li>
                            <li><a href="#">Link to page nine</a></li>
                        </ul>
                        </li>
                        <li>
                        <div class="openLevel js-openLevel">
                            Secion 1.2
                            <i class="fa fa-chevron-right"></i>
                        </div>
                        <ul class="pushNav pushNav_level js-pushNavLevel">
                            <li class="closeLevel js-closeLevel hdg">
                            <i class="fa fa-chevron-left"></i>
                            Go Back
                            </li>
                            <li><a href="#">Link to page ten</a></li>
                            <li><a href="#">Link to page eleven</a></li>
                            <li><a href="#">Link to page twelve</a></li>
                            <li><a href="#">Link to page thirteen</a></li>
                        </ul>
                        </li>
                        <li><a href="#">Link to page three</a></li>
                        <li><a href="#">Link to page four</a></li>
                    </ul>
                    </li><!-- End section 1 -->
                    
                    <li>
                    <div class="openLevel js-openLevel">
                        Section 2 
                        <i class="fa fa-chevron-right"></i>
                    </div>
                    <ul class="pushNav pushNav_level js-pushNavLevel">
                        <li class="closeLevel js-closeLevel hdg">
                        <i class="fa fa-chevron-left"></i>
                        Go Back
                        </li>
                        <li><a href="#">Link to page fourteen</a></li>
                        <li><a href="#">Link to page fifteen</a></li>
                        <li><a href="#">Link to page sixteen</a></li>
                        <li><a href="#">Link to page seventeen</a></li>
                        <li><a href="#">Link to page eighteen</a></li>
                        <li><a href="#">Link to page nineteen</a></li>
                    </ul>
                    </li>
                    <hr/>
                    <li>
                    <a href="#">Link to page one</a>
                    </li>
                    <li>
                    <a href="#">Link to page two</a>
                    </li>
                </ul>
                <div class="wrapper">
                
                <div class="burger js-menuToggle">
                    <i class="fa fa-navicon"></i>
                </div>
            </nav>

        </header>


        <!-- Main -->
        <div class="mainContainer">
            <?php echo $content; ?>
        </div>

        <!-- Footer -->
        <footer>
            <p>MyCave par Grégory Cadet-Marthe | Copyrigth</p>
        </footer>



    <script src="../public/js/menuBurger.js"></script>

    </body>
</html>