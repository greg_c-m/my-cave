<?php
$pageTitle = 'Login';
require_once('src/models/vins.php');
require_once('src/controllers/login.php');
$entreprises = getvinsHome();
ob_start();
?>

<div class="containerEdite">
    <div class="cardEdite">
        <?php if(isset($messageErreur)) { ?>
 
        <span>
            <?php echo $messageErreur ?>
        </span>
        <?php } ?>
 
        <div class="">
            <h2>Se Connecter</h2>
            <form class="formEdite" action="login.php" method="POST">
                <input type="mail" required id="mail" name="mail" placeholder="Adresse mail"><br>
 
                <input type="password" id="password" name="password" placeholder="Mot de passe"><br>
 
                <input class="btn" type="submit" value="Connexion" name="connexion">
            </form>
        </div>
    </div>
</div>

<?php 

$content = ob_get_clean();
require_once('templates/layout.php'); 
?>