<?php
$pageTitle = 'Détail du vin';
require_once('src/models/vins.php');
$vins = getDetailVins($_GET['id_vins']);

ob_start();
?>
 



<div class="containerDetail">
    <div class="bgCardDetail">
        <div class="cardsDetail">
            <div class="header">
                <div class="nameBouteils">
                    <h1><?php echo $vins['name'] ?></h1>
                </div>
            </div>
            <div class="mainCardDetail">
                <div>
                    <img class="imgCard" src="public/img/<?php echo $vins['images'] ?>" alt="photo" name="images">
                </div>
                <div class="petDetail">
                    <div class="details">
                        <div>
                            <h2>Année</h2>
                            <h3><em><?php echo $vins['year'] ?></em></h3>
                        </div>
                        <div>
                            <h2>Grappe</h2>
                            <h3><em><?php echo $vins['grapes'] ?></em></h3>
                        </div>
                        <div>
                            <h2>Pays</h2>
                            <h3><em><?php echo $vins['country'] ?></em></h3>
                        </div>
                        <div>
                            <h2>Région</h2>
                            <h3><em><?php echo $vins['region'] ?></em></h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="cardFooter">
                <h2>Description :</h2> 
                <p class="detail">
                <em><?php echo $vins['description'] ?></em>
                </p>
            </div>
        </div>
    </div>
</div>   
    
    







<?php 

$content = ob_get_clean();
require_once('templates/layout.php'); 
?>