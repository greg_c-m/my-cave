<?php
$pageTitle = 'Ajouter un utilisateurs';
require_once('src/controllers/utilisateurs_add.php');

ob_start();
?>

<div class="containerEdite">
    <div class="cardEdite">
        <h1 class="text-primary text-uppercase">Ajouter un utilisateurs</h1>
        <?php if(isset($msgError)) { ?>
            <span class="alert alert-danger mb-3"><?php echo $msgError ?></span>
        <?php } ?>
        <form class="formEdite" action="utilisateurs_add.php" method="post" enctype="multipart/form-data">
            <div class="form-floating">
                <input type="text" class="form-control" id="floatingName" name="name" placeholder="Nom">
            </div>
            <div class="form-floating">
                <input type="email" class="form-control" id="floatingYear" name="mail" placeholder="mail">
            </div>
            <?php if(isset($err_email)){echo $err_email;} ?>
            <div class="form-floating">
                <input type="password" class="form-control" id="floatingGrapes" name="password" placeholder="password">
            </div>
            <div class="btnEdit">
                <button type="submit" name="créer" class="btn">Créer</button>
            </div>
        </form>
    </div>
</div>

<?php
$content = ob_get_clean();
require_once('templates/layout.php');
?>