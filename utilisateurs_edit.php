<?php
$pageTitle = 'Editer un utilisateur';
require_once('src/controllers/utilisateurs_edit.php');

ob_start();
?>
<div class="containerEdite">
    <div class="cardEdite">
        <h1 class="text-primary text-uppercase">Editer un utilisateurs</h1>
        <?php if(isset($msgError)) { ?>
            <span class="alert alert-danger mb-3"><?php echo $msgError ?></span>
        <?php } ?>
        <form class="formEdite" action="utilisateurs_list.php?id_utilisateurs=<?php echo $utilisateurs['id'] ?>" method="POST" enctype="multipart/form-data">
            
            <div class="form-floating mb-3">
                Nom 
                <input type="text" class="form-control" id="floatingName"  name="name" placeholder="<?php echo $utilisateurs['name'] ?>">
            </div>
            <div class="form-floating">
                Mail 
                <input type="email" class="form-control" id="floatingYear" name="mail" placeholder="<?php echo $utilisateurs['mail'] ?>">
            </div>
            <?php if(isset($msg_error)){echo $msg_error;} ?>
            <?php if(isset($err_email)){echo $err_email;} ?>
            <div class="form-floating">
                Password 
                <input type="password" class="form-control" id="floatingCountry" name="password" placeholder="<?php echo $utilisateurs['password'] ?>">
            </div>
            <div class="btnEdit">
                <button type="submit" name="modifier" class="btn">Modifier</button>
            </div>
        </form>
    </div>
</div>
<?php
$content = ob_get_clean();
require_once('templates/layout.php');
?>