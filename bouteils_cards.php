<?php
$pageTitle = 'Cartes des Bouteilles';
require_once('src/models/vins.php');
$vins = getVinsHome();
ob_start();
?>
 


<div class="bgCards">
        <?php foreach ($vins as $vins) : ?>
        <div class="cardsContainer">
            <div class="header">
                <div class="naImg">
                    <div class="imgC">
                        <img class="imgCards" src="public/img/<?php echo $vins['images'] ?>" alt="photo" name="images">
                    </div>
                    <h3>Nom : <?php echo $vins['name'] ?></h3>
                </div>
            </div>
            <div class="mainCards">
                <div class="division">
                    <div class="anGrap">
                        <h4>Année : <?php echo $vins['year'] ?></h4>
                        <h4>Grape : <?php echo $vins['grapes'] ?></h4>
                    </div>
                    <div class="payReg">
                        <h4>Pays : <?php echo $vins['country'] ?></h4>
                        <h4>Région : <?php echo $vins['region'] ?></h4>
                    </div>
                </div>
                <div class="btnDetail">
                    <a href="wine_detail.php?id_vins=<?php echo $vins['id'] ?>" class="btn">detail</a>
                </div>
            </div>
        </div>
        <?php endforeach; ?>
    </div>







<?php 

$content = ob_get_clean();
require_once('templates/layout.php'); 
?>