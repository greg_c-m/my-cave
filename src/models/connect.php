<?php

try {

    /**
     * Création d'une instance de PDO afin de se connecter à notre bdd
     * type de la : mysql
     * host : localhost
     * dbname : les_bonnes_pages
     * charset : utf8 afin de prendre en compte les caractères spéciaux
     * 
     * deuxième parmètre : user
     * troisième paramètre : mot de passe, si pas de mot de passe mettre une 
     * chaine vide
     * quatrième paramètre : options sous la forme d'un tableau
     *  nous rajoutons PDO::ERRMODE_EXCEPTION afin de remonter les erreurs SQL
     */


    
    $pdo = new PDO(
        'mysql:host=localhost;dbname=mycave;charset=utf8',
        'root',
        '',
        [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        ]
    );
    
    // $pdo = new PDO(
    //     'mysql:host=localhost;dbname=id16415291_mycave;charset=utf8',
    //     'id16415291_gregorycm',
    //     'lM=nXK|46N/HrvF6',
    //     [
    //         PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    //     ]
    // );
    
    /**
     * Utilisation de la méthode query afin de faire notre select
     * On récupère l'id, nom, description, web_site, img
     * id et img ont des valeurs par défaut.
     * 
     * On stock le résultat dans notre variable $entreprises, qui permettra de
     * faire notre boucle foreach.
     * Les noms des indexs seront les noms de nos colonnes SQL
     */

} catch (\PDOException $th) {

    /**
     * Si erreur PDO, alors nous l'affichons
     */
    echo $th->getMessage();
}