<?php

function getPdoConnect(){
    require('connect.php');
    
    return $pdo;
}

function getVinsHome(){
    try {
        $pdo = getPdoConnect();

        /**
         * Utilisation de la méthode query afin de faire notre select
         * On récupère l'id, nom, description, web_site, img
         * id et img ont des valeurs par défaut.
         * 
         * On stock le résultat dans notre variable $entreprises, qui permettra de
         * faire notre boucle foreach.
         * Les noms des indexs seront les noms de nos colonnes SQL
         */
        return $pdo->query('SELECT id, name, description, images, year, region, country, grapes FROM mes_vins ORDER BY create_time DESC LIMIT 6;');
    } catch (\PDOException $th) {

        /**
         * Si erreur PDO, alors nous l'affichons
         */
        return false;
    }
}

function getVinsList(){

    try {

        /**
         * retourn la variable $pdo
         */
        $pdo = getPdoConnect();

        /**
         * Utilisation de la méthode query afin de faire notre select
         * On récupère l'id, nom, description, web_site, img
         * id et img ont des valeurs par défaut.
         * 
         * On stock le résultat dans notre variable $entreprises, qui permettra de
         * faire notre boucle foreach.
         * Les noms des indexs seront les noms de nos colonnes SQL
         */
        return $pdo->query('SELECT id, name, images, year, region, country FROM mes_vins ORDER BY create_time DESC;');
    } catch (\PDOException $th) {

        /**
         * Si erreur PDO, alors nous l'affichons
         */
        return false;
    }
}

function deleteBouteils($id_vins){
    $pdo = getPdoConnect();

    /**
     * Afin de securiser notre req SQL, nous utiliserons la méthode prepare
     * Elle permet d'éviter les injections SQL.
     * 
     * Nous préparons une Req avec les éléments venant de notre formualaire
     */
    $maReqPreparee = $pdo->prepare("DELETE FROM `mes_vins` WHERE id = :id LIMIT 1");

    /**
     * Nous récupérons notre req préparée ($maReqPreparee) afin d'y lier les paramétres 
     * aux variables
     */

    $maReqPreparee->bindParam(':id', $id_vins);

    /**
     * On execute notre requete
     */
    return $maReqPreparee->execute();
}

function getBottlePicture($id_vins){
    $pdo = getPdoConnect();
 
    $stmt = $pdo->prepare('SELECT images FROM mes_vins WHERE id = :id');
    $stmt->bindParam(':id', $id_vins);
    $stmt->execute();
 
    return $stmt->fetch(PDO::FETCH_ASSOC);
}

function getDetailVins($id_vins){
    $pdo = getPdoConnect();

    $stmt = $pdo->prepare('SELECT id, name, description, images, year, region, country, grapes FROM mes_vins WHERE id = :id');
    $stmt->bindParam(':id', $id_vins);
    $res = $stmt->execute();
    if($res){
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }else{
        return [];
    }
}

function updateVins($id_vins, $upateValues){
    $pdo = getPdoConnect();

    $stmt = $pdo->prepare('UPDATE mes_vins SET  name = :name, description =:description, year = :year, images = :images, country = :country, grapes = :grapes, description = :description WHERE id = :id');
    $stmt->bindParam(':name',$upateValues['name']);
    $stmt->bindParam(':description',$upateValues['description']);
    $stmt->bindParam(':year',$upateValues['year']);
    $stmt->bindParam(':images', $upateValues['images']);
    $stmt->bindParam(':region', $upateValues['region']);
    $stmt->bindParam(':country', $upateValues['country']);
    $stmt->bindParam(':grapes', $upateValues['grapes']);
    $stmt->bindParam(':id', $id_vins);

    return $stmt->execute();
}

/**
 * Cette fonction permetra de créer une entreprie
 * depuis un tableau qui contiendra : 
 *  le nom
 *  la description
 *  le site web
 */
function createVins($vinsValues){

    /**
     * Récupurer le connecteur pdo.
     */
    $pdo = getPdoConnect();

    /**
     * Afin de securiser notre req SQL, nous utiliserons la méthode prepare
     * Elle permet d'éviter les injections SQL.
     * 
     * Car nous préparons une Req avec les éléments venant de notre formualaire
     */
    $maReqPreparee = $pdo->prepare("INSERT INTO mes_vins(name, description, year, images, grapes, country, region) 
    VALUES (:name, :description, :year, :images, :grapes, :region, :country)");

    /**
     * Nous récupérons notre req préparée ($maReqPreparee) afin d'y lier les paramétres 
     * aux variables
     */
    $maReqPreparee->bindParam(':name', $vinsValues['name']);
    $maReqPreparee->bindParam(':description', $vinsValues['description']);
    $maReqPreparee->bindParam(':year', $vinsValues['year']);
    $maReqPreparee->bindParam(':grapes', $vinsValues['grapes']);
    $maReqPreparee->bindParam(':images', $vinsValues['images']);
    $maReqPreparee->bindParam(':country', $vinsValues['country']);
    $maReqPreparee->bindParam(':region', $vinsValues['region']);

    /**
     * On execute notre requete
     * true: si ok
     * false : si ko
     */
    return $maReqPreparee->execute();
}

function getVinsFrance() {
    $pdo = getPdoConnect();

    $maReqPreparee = $pdo->prepare("SELECT id, name, images, year, region, country, grapes FROM mes_vins WHERE country='france' ORDER BY create_time DESC");
    $maReqPreparee->execute();
    return $maReqPreparee->fetchALL(PDO::FETCH_ASSOC);
} 

function getVinsUsa() {
    $pdo = getPdoConnect();

    $maReqPreparee = $pdo->prepare("SELECT id, name, images, year, region, country, grapes FROM mes_vins WHERE country='usa' ORDER BY create_time DESC");
    $maReqPreparee->execute();
    return $maReqPreparee->fetchALL(PDO::FETCH_ASSOC);
} 

function getVinsUAutresVins() {
    $pdo = getPdoConnect();

    $maReqPreparee = $pdo->prepare("SELECT id, name, images, year, region, country, grapes FROM mes_vins WHERE country NOT IN ('france', 'usa') ORDER BY create_time DESC");
    $maReqPreparee->execute();
    return $maReqPreparee->fetchALL(PDO::FETCH_ASSOC);
}