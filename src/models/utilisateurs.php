<?php

function getPdoUtilisateurs(){
    require('connect.php');
    
    return $pdo;
}

// function getUtilisateursHome(){
//     try {
//         $pdo = getPdoUtilisateurs();

//         return $pdo->query('SELECT id, name, mail, password FROM mes_vins ORDER BY create_time DESC LIMIT 6;');
//     } catch (\PDOException $th) {

//         /**
//          * Si erreur PDO, alors nous l'affichons
//          */
//         return false;
//     }
// }

function getUtilisateursList(){

    try {

        /**
         * retourn la variable $pdo
         */
        $pdo = getPdoUtilisateurs();

        /**
         * Utilisation de la méthode query afin de faire notre select
         * On récupère l'id, nom, description, web_site, img
         * id et img ont des valeurs par défaut.
         * 
         * On stock le résultat dans notre variable $entreprises, qui permettra de
         * faire notre boucle foreach.
         * Les noms des indexs seront les noms de nos colonnes SQL
         */
        return $pdo->query('SELECT id, name, mail FROM utilisateurs ORDER BY create_time DESC;');
    } catch (\PDOException $th) {

        /**
         * Si erreur PDO, alors nous l'affichons
         */
        return false;
    }
}

function getDetailUtilisateurs($id_utilisateurs){
    $pdo = getPdoUtilisateurs();

    $stmt = $pdo->prepare('SELECT id, name, mail, password FROM utilisateurs WHERE id = :id');
    $stmt->bindParam(':id', $id_utilisateurs);
    $res = $stmt->execute();
    if($res){
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }else{
        return [];
    }
}

function deleteUtilisateurs($id_utilisateurs){
    $pdo = getPdoUtilisateurs();

    /**
     * Afin de securiser notre req SQL, nous utiliserons la méthode prepare
     * Elle permet d'éviter les injections SQL.
     * 
     * Nous préparons une Req avec les éléments venant de notre formualaire
     */
    $maReqPreparee = $pdo->prepare("DELETE FROM `utilisateurs` WHERE id = :id LIMIT 1");

    /**
     * Nous récupérons notre req préparée ($maReqPreparee) afin d'y lier les paramétres 
     * aux variables
     */

    $maReqPreparee->bindParam(':id', $id_utilisateurs);

    /**
     * On execute notre requete
     */
    return $maReqPreparee->execute();
}

function updateUtilisateurs($id_utilisateurs, $updateValues){
    $pdo = getPdoUtilisateurs();

    $stmt = $pdo->prepare('UPDATE utilisateurs SET name = :name, mail = :mail, password = :password  WHERE id = :id');
    $stmt->bindParam(':name',$updateValues['name']);
    $stmt->bindParam(':mail',$updateValues['mail']);
    $stmt->bindParam(':password',$updateValues['password']);
    $stmt->bindParam(':id', $id_utilisateurs);

    return $stmt->execute();
}

function createUtilisateurs($utilisateursValues){

    /**
     * Récupurer le connecteur pdo.
     */
    $pdo = getPdoUtilisateurs();

    /**
     * Afin de securiser notre req SQL, nous utiliserons la méthode prepare
     * Elle permet d'éviter les injections SQL.
     * 
     * Car nous préparons une Req avec les éléments venant de notre formualaire
     */
    $maReqPreparee = $pdo->prepare("INSERT INTO `utilisateurs`(`name`, mail, password) 
    VALUES (:name, :mail, :password)");

    /**
     * Nous récupérons notre req préparée ($maReqPreparee) afin d'y lier les paramétres 
     * aux variables
     */
    $maReqPreparee->bindParam(':name',$utilisateursValues['name']);
    $maReqPreparee->bindParam(':mail',$utilisateursValues['mail']);
    $maReqPreparee->bindParam(':password',$utilisateursValues['password']);

    /**
     * On execute notre requete
     * true: si ok
     * false : si ko
     */
    return $maReqPreparee->execute();

}