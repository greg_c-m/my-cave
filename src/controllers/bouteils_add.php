<?php

/**
 * Controlleur de l'entreprise_add.html
 * Permet d'appeler le model (faire les requêtes)
 * 
 * Son but est d'ajouter un nouvel utilisateur quand
 * nous validons le formulaire
 */

 /**
  * Inclure le fichier model qui contiendra toutes les fonctions
  * SQL vers la table entreprise.
  */
require_once('src/models/vins.php');
require_once('secure.php');

/**
 * J'enregistre une nouvelle entreprise si et seulement si j'ai 
 * validé mon formulaire.
 * Pour le savoir je regarde si le name du bouton de type
 * submit est présent dans ma variable $_POST à savoir "creer"
 */

if (isset($_POST['creer'])) {

    if($_FILES['images']['name'] == ''){
        $image = 'default.jpg';
    }else{
        $image = $_FILES['images']['name'];
    }
    
    $vinsValues = [
        'name' => html($_POST['name']),
        'description' => html($_POST['description']),
        'year' => html($_POST['year']),
        'region' => html($_POST['region']),
        'grapes' => html($_POST['grapes']),
        'country' => html($_POST['country']),
        'images' => $image
    ];

    $name = html($_POST['name']); 
    $description = html($_POST['description']); 
    $year = html($_POST['year']); 
    $region = html($_POST['region']); 
    $grapes = html($_POST['grapes']); 
    $country = html($_POST['country']); 

    if(longeurDeCaractere(50, $name)===false){
        return $errorName = '<p>Merci de mettre moins de 50 caractères.</p>';
    }
    if(longeurDeCaractere(50, $grapes)===false){
        return $errorGrapes = '<p>Merci de mettre moins de 50 caractères.</p>';
    }
    if(longeurDeCaractere(50, $country)===false){
        return $errorCountry = '<p>Merci de mettre moins de 50 caractères.</p>';
    }
    if(longeurDeCaractere(50, $region)===false){
        return $errorRegion = '<p>Merci de mettre moins de 50 caractères.</p>';
    }

    if(imageWeight($_FILES ['images'])===false){
        return $errorImageWeight = '<p>La taille du fichier est limité à 1 Mo, merci de remplacer le fichier </p>';
    }
    if(imagesExtention($_FILES['images'])===false){
        return $errorExtention = '<p>Merci de mettre une image.</p>';
    }


    /**
     * Pour créer une entreprise, j'appele la fonction 
     * createEntreprise et je lui passe un tableau avec tous les 
     * éléments dont a besoin ma requête.
     * 
     * Dans $_POST(qui est de type tableau), je vais retrouver
     * tous les valeurs des champs input de mon formulaire.
     * 
     * A savoir que $_POST se retrouvera dans ma fonction sous le nom
     * de $entrepriseValues
     */
    
    $res = createVins($vinsValues);
    if($_FILES['image']['name'] != ''){
        move_uploaded_file($_FILES['image']['tmp_name'], './public/img/'.$image);
    }
    
    

    /**
     * Si $res est true alors la création est ok
     * Si $res set false alors problème lors de la création
     */
    if ($res) {
        /**
         * Nous redirigeons vers la page index afin de ne pas ajouter d'entreprise
         * avec une actualisation
         * 
         * Le exit est important car il empèche d'excuter le code qui suit le header.
         */
        header('Location: ./bouteils_list.php');
        exit;
    }
}
