<?php

/**
 * Le but de cette page est de vérifier si un utilisateur est connecté pour les pages
 * qui ont en besoin.
 * Si une page demande une connection et que l'utilisateur ne l'est pas, alors redirection
 * vers login.phtml
 *  
 * Récupération du fichier html en cours
 * pour cela nous utiliserons la variable $_SERVER qui contient toutes les informations serveur
 * A l'index PHP_SELF, nous aurons l'url sans le host et sans les paramètres $_GET
  */

//ex url de départ : http://localhost/ecod_2021/entreprise_add.html

$phpSelf = $_SERVER['PHP_SELF'];
//phpself = /ecod_2021/entreprise_add.html

/**
 * Afin de récupérer le nom du fichier nous utilisons basename pour remonter la composante 
 * finale du chemin
 * 
 * $currentPage devrait avoir pour valeur le nom de mon ficher html
 */
$fileUrl = basename($phpSelf);
//fileUrl = entreprise_add.html

$currentPage = pathinfo($fileUrl)['filename'];
//currentPage = entreprise_add

/** 
 * Déclarer un tableau qui va contenir l'ensemble des pages demandant une connexion
 * sans faire de distinction entre les php / html
*/
$tabPageAdmin = [
    'utilisateurs_list',
    'bouteils_add'
];

/**
 * chercher l'index du nom de la page dans notre tableau
 * Si non présent alors false est retourné.
 */
$res = in_array($currentPage, $tabPageAdmin);

/**
 * Si array_serach retourne autre chose que false (c-a-d un index) 
 * Et que ma $_SESSION n'a pas d'index 'utilisteur'
 * Alors ce voudrait dire que je veux acceder à une page demandant une connexion sans être
 * connecté.
 * 
 * Dans ce cas là, je redirige vers login.phtml
 */
if($res == true && !isset($_SESSION['utilisateurs'])){
    header('Location: ./login.php');
    exit;
}