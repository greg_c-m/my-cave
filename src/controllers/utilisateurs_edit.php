<?php
// session_start();

/**
 * Cette page va afficher les informations d'une entreprise et permettre leurs modifications
 * Pour cela :
 * - on récupère les informations enregistrées en bdd de l'entreprise
 * - tester si nous validons le formulaire 
 *      - si oui : 
 *                  - enregistrer les éléments en bdd
 *                  - renvoyer vers la page entreprise liste                  
 *      - si non : ne rien faire et afficher le formuaire
 */
require_once('src/models/utilisateurs.php');
require_once('secure.php');

$utilisateurs = getDetailUtilisateurs($_GET['id_utilisateurs']);

if(isset($_POST['modifier'])){

$updateValues = [
    'name' => html($_POST['name']),
    'mail' => html($_POST['mail']),
    'password' => html($_POST['password']),
];
/**
 * Nous récupérons le détail de l'entreprise afin d'afficher les informations
 * de l'entreprise. En effet pour modifier il est plus simple d'avoir les 
 * informations.
 * 
 * Pour récupérer les informations, nous utilisons la valeur de 
 * l'id_entreprise que nous passons à notre fonction getDetailEntreprise.
 * Celle-ci nous retournera :  id, name, description, web_site, img (ce 
 * que nous afficherons)
 */
$utilisateurs = updateUtilisateurs($_GET['id_utilisateurs'], $updateValues);


if(empty($mail) || empty($password)) {
    $msg_error = 'Merci de remplir les champs manquants';
}
if(emailVerify($mail)===false){
    return $err_email = '<p>Veuillez mettre un email valide.</p>';
}
/**
 * Si entreprise == false alors l'entreprise n'a pas été trouvée par mon SQL
 * on redirige vers la liste des entrirpses avec un msg 
 */
if(!$utilisateurs){
    $_SESSION['msg_flash'] = 'Utilisateur non connue';

    header('Location: ./utilisateurs_list.php');
    exit;
}
}