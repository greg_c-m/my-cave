<?php
/**
 * On démarre la session
 */
session_start();

// définir la fonction de connection
function connexion($mail, $password) {
    require('src/models/connect.php');
    //creation de la requete
    $requete = $pdo->prepare('SELECT id, mail, password FROM utilisateurs WHERE mail = :mail');
 
    //donner de parametres aux variables de la requete
    $requete ->bindParam(':mail', $mail);
    $requete->execute();
    $res = $requete-> fetch(PDO::FETCH_ASSOC );

    if ($res && (password_verify($password, $res['password']))) 
    {
        return true;
    }
    else {
        return false;
    }
}
/**
 * Si l'index de session utilisateur est définit alors 
 * il y a déjà une connexion.
 */
if(isset($_SESSION['utilisateurs'])){
    header('Location: ./index.php');
    exit;
}
require_once('secure.php');

if(isset($_POST['connexion'])){
    require_once('src/models/utilisateurs.php');
    
    $user = connexion(html($_POST['mail']), html($_POST['password']));

    if($user == false){
        /**
         * Si User == false, alors cel veut dire :
         *  - couple email/password inconnu
         *  - problème sur la req
         * 
         * Ajout d'un message d'erreur
         */
        $msgError =  'Veuillez vérifier vos paramètres de connexion';
    }else{
        /**
         * Si User == true
         * J'enregistre les informations user dans l'index 'utilisateur' de la 
         * variable (qui est de type tableau) $_SESSION
         */
        $_SESSION['utilisateurs'] = $user;
        
        /**
         * Enregister un message indiquant que tout est Ok
         */
        $_SESSION['msg_flash'] = 'Connexion ok';

        /**
         * Le traitement est terminé je redirige vers la page index.
         */
        header('Location: ./index.php');
        exit;
    }
}