<?php
// session_start();

/**
 * Cette page va afficher les informations d'une entreprise et permettre leurs modifications
 * Pour cela :
 * - on récupère les informations enregistrées en bdd de l'entreprise
 * - tester si nous validons le formulaire 
 *      - si oui : 
 *                  - enregistrer les éléments en bdd
 *                  - renvoyer vers la page entreprise liste                  
 *      - si non : ne rien faire et afficher le formuaire
 */
require_once('src/models/vins.php');
require_once('secure.php');

/**
 * Nous récupérons le détail de l'entreprise afin d'afficher les informations
 * de l'entreprise. En effet pour modifier il est plus simple d'avoir les 
 * informations.
 * 
 * Pour récupérer les informations, nous utilisons la valeur de 
 * l'id_entreprise que nous passons à notre fonction getDetailEntreprise.
 * Celle-ci nous retournera :  id, name, description, web_site, img (ce 
 * que nous afficherons)
 */
$vins = getDetailVins($_GET['id_vins']);

/**
 * Si entreprise == false alors l'entreprise n'a pas été trouvée par mon SQL
 * on redirige vers la liste des entrirpses avec un msg 
 */
if(!$vins){
    $_SESSION['msg_flash'] = 'Vin non connue';

    header('Location: ./bouteils_list.php');
    exit;
}



/**
 * Si l'utilisateur clique sur le bouton modifier, nous regardons si nous avons un 
 * index du "modifier" dans notre tableau $_POST.
 * Nous avons donné ce name au bouton : 
 * <button type="submit" name="modifier" class="btn btn-primary mt-3">Modifier</button>
 * 
 * Pour tester si un index existe nous utilisons la fonction PHP isset()  qui retourne :
 * true : si l'index existe 
 * false : si l'index n'existe pas
 * 
 * Si true, alors nous prendrons en compte les valeurs du formualaire afin de mettre à jour notre bdd
 * Si false, alors nous ne faisons rien car nous n'avaons pas cliqué sur le bouton
 * "modifier" du formualaire.
 */
if(isset($_POST['modifier'])){

    /**
     * Regarder si une image à été postée. Pour cela il faut : 
     *  - que votre formulaire possède l'attribut : enctype="multipart/form-data"
     *  - avoir un input de type FILE
     *  - Attention si l'attribut enctype="multipart/form-data" n'est pas présent alors votre input FILE sera
     * considéré comme un input de type TEXT et retournera que le nom du fichier dans la variable $_POST/$_GET
     * 
     * Sinon nous récupérons les informations du fichier dans $_FILES.
     * $Files avec les indexs de chaque name de type input.
     * Dans notre cas : <input class="form-control" type="file" id="image" name="image">
     * le name a pour valeur "image"
     * La variable $FILES va contenir les informations suivantes :
     * [name] => nom du fichier
     * [type] => type de fichier (ex:image/jpeg)
     * [tmp_name] => son chemin sur le serveur (répertoir temporair)
     * [error] => code d'erreur
     * [size] => taille du fichier en octet
     */
    if($_FILES['images']['name'] == ''){
        /**
         * Si le nom du fichier est vide, cela veut dire qu'il n'y a pas de fichier
         * Alors on récupère le nom stocké en bdd, nous avons récupéré ces informations 
         * grace a getDetailEntreprise()
         * 
         */
        $image = $vins['images'];
    }else{
        /**
         * Si le nom du fichier n'est pas vide alors on le stocke dans notre variable image
         */
        $image = $_FILES['images']['name'];
    }
    
    /**
     * Création d'une variable de type array avec tous les éléments pour l'update
     * name, description et web_site proviennent du formulaire ($_POST)
     * img soit du $_FILES si il y avait une image, soit nous gardons le nom précédent
     */
    $vinsValues = [
        'name' => html($_POST['name']),
        'description' => html($_POST['description']),
        'year' => html($_POST['year']),
        'grapes' => html($_POST['grapes']),
        'country' => html($_POST['country']),
        'region' => html($_POST['region']),
        'images' => $image
    ];

    $name = html($_POST['name']); 
    $description = html($_POST['description']); 
    $year = html($_POST['year']); 
    $region = html($_POST['region']); 
    $grapes = html($_POST['grapes']); 
    $country = html($_POST['country']); 

    if(longeurDeCaractere(50, $name)===false){
        return $errorName = '<p>Merci de mettre moins de 50 caractères.</p>';
    }
    if(longeurDeCaractere(50, $grapes)===false){
        return $errorGrapes = '<p>Merci de mettre moins de 50 caractères.</p>';
    }
    if(longeurDeCaractere(50, $country)===false){
        return $errorCountry = '<p>Merci de mettre moins de 50 caractères.</p>';
    }
    if(longeurDeCaractere(50, $region)===false){
        return $errorRegion = '<p>Merci de mettre moins de 50 caractères.</p>';
    }
    if(imageWeight($_FILES ['images'])===false){
        return $errorImageWeight = '<p>La taille du fichier est limité à 1 Mo, merci de remplacer le fichier </p>';
    }
    if(uploadImages($_FILES['images'])===false){
        return $errorUploadImage = '<p>Merci de mettre une image.</p>';
    }

    /**
     * Mise à jour de l'entreprise en bdd avec l'id récupérée dans l'url ($_GET)
     * et le tableau que nous vennons de créer
     */
    $res = updateVins($_GET['id_vins'], $vinsValues);

    if($res){

        if($_FILES['images']['name'] != ''){
            /**
             * Si non de fichier alors on déplace le fichier dans notre archi
             * Nous utilisons move_uploaded_file()
             *      - premier param : le path actuel
             *      - deuxieme param : le path cible
             * Attention les chemins doivent aussi contenir le nom du fichier (ne pas oublier dans la cible)
             */
            move_uploaded_file($_FILES['images']['tmp_name'], './public/img/'.$image);
            if($vins['images'] != 'default.jpg'){
                /**
                 * unlink permet de supprimer un fichier si celui-ci n'est pas notre default
                 * Nous supprimer l'ancienne image si ce n'est pas default.jpg
                 */
                unlink('./public/img/'.$vins['images']);
            }
        }

        $_SESSION['msg_flash'] = 'Modification de la bouteille.';
        
        header('Location: ./bouteils_list.php');
        exit;
    }else{
        $msgError = 'Problème lors de la modification';
    }
}
