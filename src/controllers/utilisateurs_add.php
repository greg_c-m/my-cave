<?php

/**
 * Controlleur de l'entreprise_add.html
 * Permet d'appeler le model (faire les requêtes)
 * 
 * Son but est d'ajouter un nouvel utilisateur quand
 * nous validons le formulaire
 */

 /**
  * Inclure le fichier model qui contiendra toutes les fonctions
  * SQL vers la table entreprise.
  */
require_once('src/models/utilisateurs.php');
require_once('secure.php');

/**
 * J'enregistre une nouvelle entreprise si et seulement si j'ai 
 * validé mon formulaire.
 * Pour le savoir je regarde si le name du bouton de type
 * submit est présent dans ma variable $_POST à savoir "creer"
 */

if (isset($_POST['creer'])) {

    
    $utilisateursValues = [
        'name' => html($_POST['name']),
        'mail' => html($_POST['mail']),
        'password' => password_hash(html($_POST['password']), PASSWORD_DEFAULT)
    ];

    $name = html($_POST['name']);
    $mail = html($_POST['mail']);
    $password = password_hash(html($_POST['password']), PASSWORD_DEFAULT);
    $pattern = '/^(?=.*\d+)(?=.*\W+)(?=.*[A-Z]+)(?=.*[a-z]+)[a-zA-Z\d\W]{8,}$/';

    if(empty($mail) || empty($password)) {
        $msg_error = 'Merci de remplir les champs manquants';
    }
    if(emailVerify($mail)===false){
        return $err_email = '<p>Veuillez mettre un email valide.</p>';
    }
    elseif(!preg_match($pattern, $password)) {
        $msg_error = 'Votre mot de passe doit comporter au moins 8 caractères, une majuscule, un caractère spécial et un chiffre';
    }

    // else {
    //     $req = $db->prepare("
    //         SELECT password, id
    //         FROM utilisateurs
    //         WHERE mail = :mail
    //     ");
    //     $req->bindValue(':mail', filter_var($mail,FILTER_VALIDATE_EMAIL), PDO::PARAM_STR);
    //     $req->execute();
    //     $data = $req->fetchObject();
    
    //     if(!$data || !password_verify($password, $data->password)) {
    //         $msg_error = 'Votre email ou mot de passe ne correspondent pas';
    //     }
    //     elseif($data || password_verify($password, $data->password)) {
    //         $msg_success = 'Vous êtes connecté';
    //         $_SESSION['id'] = $data->id;
    //     }
    // }
    
    $result = isset($msg_error);
    $msg = array();
    if($result) {
        $msg['error'] = TRUE;
        $msg['msg'] = $msg_error;
    }
    else {
        $msg['error'] = FALSE;
        $msg['msg'] = $msg_success;
    }
    $msg['url'] = 'http://localhost/my_cave_projet/';
    

    /**
     * Pour créer une entreprise, j'appele la fonction 
     * createEntreprise et je lui passe un tableau avec tous les 
     * éléments dont a besoin ma requête.
     * 
     * Dans $_POST(qui est de type tableau), je vais retrouver
     * tous les valeurs des champs input de mon formulaire.
     * 
     * A savoir que $_POST se retrouvera dans ma fonction sous le nom
     * de $entrepriseValues
     */
    $res = createUtilisateurs($utilisateursValues);
    

    /**
     * Si $res est true alors la création est ok
     * Si $res set false alors problème lors de la création
     */
    if ($res) {
        /**
         * Nous redirigeons vers la page index afin de ne pas ajouter d'entreprise
         * avec une actualisation
         * 
         * Le exit est important car il empèche d'excuter le code qui suit le header.
         */
        header('Location: ./utilisateurs_list.php');
        exit;
    }
}