<?php

// faille XSS
function html($string){
	return trim(htmlspecialchars($string, ENT_QUOTES));
}
//métrise de la longeur des chaine de caractères
function longeurDeCaractere(int $char, string $champs){
    if(strlen($champs)>$char){
        return false;
    }
}

//taille image
function imageWeight($image){
if($image['error'] === 1 || $image['error'] === 2) {
    return false;
}
if($image['size'] > 1048576) {
    $msg_error = 'La taille du fichier est limité à 1 Mo, merci de remplacer le fichier';
    return false;
}
}
//extention images 
function imagesExtention($image){
    $ext = ['png', 'jpg', 'jpeg', 'gif'];
    if(!in_array(pathinfo($image['name'],PATHINFO_EXTENSION), $ext)) {
        return false;
    }
}
//modification images
function uploadImages($image){
    if($image['error'] === UPLOAD_ERR_PARTIAL || $image['error'] > UPLOAD_ERR_NO_FILE) {
        return false;
    }
}
function emailVerify($email){
    if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    return false;
}
}

?>