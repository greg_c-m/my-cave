<?php

require_once('../models/vins.php');

if(isset($_GET['id_vins'])){
    $id_vins = $_GET['id_vins'];
    $selectPictureToDelete = getBottlePicture($id_vins);
    $isDeleted = deleteBouteils($id_vins);
}else{
    $isDeleted = true;
}


if ($isDeleted) {

    unlink('./public/img/'.$selectPictureToDelete['picture']);
    /**
     * Nous redirigeons vers la page index afin de ne pas ajouter d'entreprise
     * avec une actualisation
     * 
     * Le exit est important car il empèche d'excuter le code qui suit le header.
     */
    header('Location: /formation/my_cave_projet/bouteils_list.php');
    exit;
} else {
    echo 'prob bob !!';
}