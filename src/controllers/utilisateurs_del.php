<?php

require_once('../models/utilisateurs.php');

if(isset($_GET['id_utilisateur'])){
    $id_utilisateurs = $_GET['id_utilisateur'];
    $isDeleted = deleteUtilisateurs($id_utilisateurs);
}else{
    $isDeleted = true;
}


if ($isDeleted) {

    /**
     * Nous redirigeons vers la page index afin de ne pas ajouter d'entreprise
     * avec une actualisation
     * 
     * Le exit est important car il empèche d'excuter le code qui suit le header.
     */
    header('Location: ../../utilisateurs_list.php');
    exit;
} else {
    echo 'prob bob !!';
}
